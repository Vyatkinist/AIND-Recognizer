import math
import statistics
import warnings

import numpy as np
from hmmlearn.hmm import GaussianHMM
from sklearn.model_selection import KFold
from asl_utils import combine_sequences


class ModelSelector(object):
    '''
    base class for model selection (strategy design pattern)
    '''

    def __init__(self, all_word_sequences: dict, all_word_Xlengths: dict, this_word: str,
                 n_constant=3,
                 min_n_components=2, max_n_components=10,
                 random_state=14, verbose=False):
        self.words = all_word_sequences
        self.hwords = all_word_Xlengths
        self.sequences = all_word_sequences[this_word]
        self.X, self.lengths = all_word_Xlengths[this_word]
        self.this_word = this_word
        self.n_constant = n_constant
        self.min_n_components = min_n_components
        self.max_n_components = max_n_components
        self.random_state = random_state
        self.verbose = verbose

    def select(self):
        raise NotImplementedError

    def base_model(self, num_states):
        # with warnings.catch_warnings():
        warnings.filterwarnings("ignore", category=DeprecationWarning)
        # warnings.filterwarnings("ignore", category=RuntimeWarning)
        try:
            hmm_model = GaussianHMM(n_components=num_states, covariance_type="diag", n_iter=1000,
                                    random_state=self.random_state, verbose=False).fit(self.X, self.lengths)
            if self.verbose:
                print("model created for {} with {} states".format(self.this_word, num_states))
            return hmm_model
        except:
            if self.verbose:
                print("failure on {} with {} states".format(self.this_word, num_states))
            return None


class SelectorConstant(ModelSelector):
    """ select the model with value self.n_constant

    """

    def select(self):
        """ select based on n_constant value

        :return: GaussianHMM object
        """
        best_num_components = self.n_constant
        return self.base_model(best_num_components)


class SelectorBIC(ModelSelector):
    """ select the model with the lowest Bayesian Information Criterion(BIC) score

    http://www2.imm.dtu.dk/courses/02433/doc/ch6_slides.pdf
    Bayesian information criteria: BIC = -2 * logL + p * logN
    """

    def select(self):
        """ select the best model for self.this_word based on
        BIC score for n between self.min_n_components and self.max_n_components

        :return: GaussianHMM object
        """
        warnings.filterwarnings("ignore", category=DeprecationWarning)
        split_method = KFold(n_splits=2) if len(self.sequences) == 2 else KFold()
        min_BIC = float("inf")
        best_n_components = 0
        best_model = None

        for n_components in range(self.min_n_components, self.max_n_components):
            logL = float("inf")
            model = None

            try:
                model = GaussianHMM(n_components=n_components, n_iter=1000).fit(self.X, self.lengths)
                logL = model.score(self.X, self.lengths)
            except ValueError:
                pass

            # skip iteration in case a model wasn't trained or score call failed
            if model == None or logL == float("inf"):
                continue

            n_features = len(self.X[0])
            p = n_components ** 2 + 2 * n_features * n_components - 1
            BIC = -2 * logL + p * np.log(len(self.lengths))

            if BIC < min_BIC:
                min_BIC = BIC
                best_n_components = n_components
                best_model = model

        return best_model


class SelectorDIC(ModelSelector):
    ''' select best model based on Discriminative Information Criterion

    Biem, Alain. "A model selection criterion for classification: Application to hmm topology optimization."
    Document Analysis and Recognition, 2003. Proceedings. Seventh International Conference on. IEEE, 2003.
    http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.58.6208&rep=rep1&type=pdf
    DIC = log(P(X(i)) - 1/(M-1)SUM(log(P(X(all but i))
    '''

    def select(self):
        warnings.filterwarnings("ignore", category=DeprecationWarning)
        split_method = KFold(n_splits=2) if len(self.sequences) == 2 else KFold()
        # max value is best because the likelihoood is being penalized by other words estimation
        # since max likelihood is best, it'll be best with or without penalty
        max_DIC = float("-inf")
        best_n_components = 0
        best_model = None

        for n_components in range(self.min_n_components, self.max_n_components):
            this_word_logL = float("-inf")
            other_words_logL_sum = 0
            # since hmmlearn can throw an exception
            # we might not be able to score every word
            words_scored = 0
            model = None

            try:
                model = GaussianHMM(n_components=n_components, n_iter=1000).fit(self.X, self.lengths)
            except ValueError:
                # do not calculate DIC if model were not created
                continue

            # calculate scores for all words in the corpus, separating training word
            for word in self.words:
                try:
                    X, lengths = self.hwords[word]
                    logL = model.score(X, lengths)

                    if word == self.this_word:
                        this_word_logL = logL
                    else:
                        other_words_logL_sum += logL
                        words_scored += 1
                except ValueError:
                    pass
            
            penalty = 0 if words_scored == 0 else other_words_logL_sum / float(words_scored)
            DIC = this_word_logL - penalty

            if DIC > max_DIC:
                max_DIC = DIC
                best_n_components = n_components
                best_model = model

        return best_model


class SelectorCV(ModelSelector):
    ''' select best model based on average log Likelihood of cross-validation folds

    '''

    def select(self):
        warnings.filterwarnings("ignore", category=DeprecationWarning)

        # since where there is only 1 word sequence, it means that there's no test set
        # no test set means that there's no way to check if a model is overfit
        # if there's no way to measure if a model is overfit, the most overfit model will win
        # therefore no point of training on a single data entry
        # let's return a model with minimal number of components in this case
        if len(self.sequences) < 2:
            return GaussianHMM(n_components=self.min_n_components, n_iter=1000).fit(self.X, self.lengths)

        split_method = KFold(n_splits=2) if len(self.sequences) == 2 else KFold()
        max_avg_logL = float("-inf")
        best_n_components = 0

        for n_components in range(self.min_n_components, self.max_n_components):
            total_logL = 0
            # since model.score can two an exception on insufficient data (like in the "FISH" word case)
            # we need to use average likelihood and track how many models were evaluated
            models_evaluated = 0

            # get indices of sequences for training and testing sets
            for cv_train_idx, cv_test_idx in split_method.split(self.sequences):
                # get train and tests sequences
                train_X, train_lengths = combine_sequences(split_index_list=cv_train_idx, sequences=self.sequences)
                test_X, test_lengths = combine_sequences(split_index_list=cv_test_idx, sequences=self.sequences)

                model = None
                try:
                    model = GaussianHMM(n_components=n_components, n_iter=1000).fit(train_X, train_lengths)
                except ValueError:
                    continue

                # get logL on a test data. Since it can throw an error on insufficient data
                # we catch an exception and disregard the data on this iteration
                try:
                    logL = model.score(test_X, test_lengths)
                    total_logL += logL
                    models_evaluated += 1
                except ValueError:
                    pass

            avg_logL = float("-inf") if models_evaluated == 0 else total_logL / float(models_evaluated)

            if avg_logL > max_avg_logL:
                max_avg_logL = avg_logL
                best_n_components = n_components

        return GaussianHMM(n_components=best_n_components, n_iter=1000).fit(self.X, self.lengths)
